package com.jpmc.theater;

import java.time.Duration;
import java.util.Objects;

/**
 * Represents a movie in the theater.
 */
class Movie {
    private final String title;
    private final Duration duration;
    private final double baseTicketPrice;
    private static final StringBuilder SB;

    static {
        SB = new StringBuilder();
    }

    /**
     * Constructs a new Movie object.
     *
     * @param title           The title of the movie.
     * @param duration        The duration of the movie.
     * @param baseTicketPrice The base ticket price for the movie.
     */
    Movie(String title, Duration duration, double baseTicketPrice) {
        this.title = title;
        this.duration = duration;
        this.baseTicketPrice = baseTicketPrice;
    }

    /**
     * Get the title of the movie.
     *
     * @return The title of the movie.
     */
    String getTitle() {
        return title;
    }

    /**
     * Get the duration of the movie.
     *
     * @return The duration of the movie.
     */
    Duration getDuration() {
        return duration;
    }

    /**
     * Get the base ticket price for the movie.
     *
     * @return The base ticket price for the movie (without any discounts or fees applied)
     */
    double getBaseTicketPrice() {
        return baseTicketPrice;
    }

    /**
     * Indicates whether some other object is "equal to" this one.
     *
     * @param o The reference object with which to compare.
     * @return {@code true} if this object is the same as the obj
     * argument; {@code false} otherwise.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Movie movie = (Movie) o;
        return Double.compare(movie.baseTicketPrice, baseTicketPrice) == 0 &&
            Objects.equals(title, movie.title);
    }

    /**
     * Returns a hash code value for the object.
     *
     * @return A hash code value for this object.
     */
    @Override
    public int hashCode() {
        return Objects.hash(title, baseTicketPrice);
    }

    /**
     * Returns a string representation of the object.
     *
     * @return A string representation of the object.
     */
    @Override
    public String toString() {
        SB.setLength(0);
        SB.append("Movie{")
            .append("title='").append(title).append('\'')
            .append(", duration=").append(duration)
            .append(", baseTicketPrice=").append(baseTicketPrice)
            .append('}');
        return SB.toString();
    }
}
