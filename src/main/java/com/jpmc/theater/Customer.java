package com.jpmc.theater;

import java.util.Objects;

/**
 * Represents a customer of the theater with a name.
 */
class Customer {
    private final String name;

    /**
     * Constructs a new Customer object with the given name.
     *
     * @param name The name of the customer.
     */
    Customer(String name) {
        this.name = name;
    }

    /**
     * Reserve a showing for a customer.
     *
     * @param theater The theater where the customer can make the reservation.
     * @param showing  The specific showing to reserve.
     * @return true if the reservation was successfully made, false if the showing trying to be reserved does not exist.
     */
    boolean reserveShowing(Theater theater, Showing showing) {
        if (!theater.getShowings().contains(showing)) {
            return false;
        }
        Reservation reservation = new Reservation(showing, this);
        theater.makeReservation(reservation);
        return theater.getReservationsForCustomer(this).contains(reservation);
    }

    /**
     * Returns a string representation of the customer.
     *
     * @return A string representation of the customer.
     */
    @Override
    public String toString() {
        return "Customer{" +
            "name='" + name + '\'' +
            '}';
    }

    /**
     * Indicates whether some other object is "equal to" this one.
     *
     * @param o The reference object with which to compare.
     * @return {@code true} if this object is the same as the obj argument;
     *         {@code false} otherwise.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return Objects.equals(name, customer.name);
    }

    /**
     * Returns a hash code value for the customer.
     *
     * @return A hash code value for this customer.
     */
    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
