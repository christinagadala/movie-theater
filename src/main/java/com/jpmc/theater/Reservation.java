package com.jpmc.theater;

import java.util.Objects;

/**
 * Represents a reservation made by a customer for a specific showing in a theater.
 */
class Reservation {
    private final Showing showing;
    private final Customer customer;
    private static final StringBuilder SB;

    static {
        SB = new StringBuilder();
    }

    /**
     * Constructs a new Reservation object.
     *
     * @param showing  The specific showing for which the reservation is made.
     * @param customer The customer who made the reservation.
     */
    Reservation(Showing showing, Customer customer) {
        this.showing = showing;
        this.customer = customer;
    }


    /**
     * Get the specific showing for which the reservation is made.
     *
     * @return The showing associated with the reservation.
     */
    Showing getShowing() {
        return showing;
    }

    /**
     * Get the customer who made the reservation.
     *
     * @return The customer who made the reservation.
     */
    Customer getCustomer() {
        return customer;
    }

    /**
     * Returns a hash code value for the object.
     *
     * @return A hash code value for this object.
     */
    @Override
    public int hashCode() {
        return Objects.hash(showing, customer);
    }

    /**
     * Indicates whether some other object is "equal to" this one.
     *
     * @param o The reference object with which to compare.
     * @return {@code true} if this object is the same as the obj argument;
     * {@code false} otherwise.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Reservation that = (Reservation) o;
        return Objects.equals(showing, that.showing) &&
            Objects.equals(customer, that.customer);
    }

    /**
     * Returns a string representation of the object.
     *
     * @return A string representation of the object.
     */
    @Override
    public String toString() {
        SB.setLength(0);
        SB.append("Reservation{")
            .append("showing=").append(showing)
            .append(", customer=").append(customer)
            .append('}');
        return SB.toString();
    }
}
