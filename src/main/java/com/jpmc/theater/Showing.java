package com.jpmc.theater;

import java.time.LocalDateTime;
import java.util.Objects;

/**
 * Represents a showing of a movie in the theater.
 */
class Showing {
    private final LocalDateTime dateTime;
    private final Movie movie;
    private static final StringBuilder SB;

    static {
        SB = new StringBuilder();
    }

    /**
     * Constructs a new Showing object.
     *
     * @param dateTime The date and time of the showing.
     * @param movie    The movie being shown.
     */
    Showing(LocalDateTime dateTime, Movie movie) {
        this.dateTime = dateTime;
        this.movie = movie;
    }

    /**
     * Get the date and time of the showing.
     *
     * @return The date and time of the showing.
     */
    LocalDateTime getDateTime() {
        return dateTime;
    }

    /**
     * Get the movie being shown.
     *
     * @return The movie being shown.
     */
    Movie getMovie() {
        return movie;
    }

    /**
     * Returns a hash code value for the object.
     *
     * @return A hash code value for this object.
     */
    @Override
    public int hashCode() {
        return Objects.hash(dateTime, movie);
    }

    /**
     * Indicates whether some other object is "equal to" this one.
     *
     * @param o The reference object with which to compare.
     * @return {@code true} if this object is the same as the obj argument;
     * {@code false} otherwise.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Showing showing = (Showing) o;
        return Objects.equals(dateTime, showing.dateTime) &&
            Objects.equals(movie, showing.movie);
    }

    /**
     * Returns a string representation of the object.
     *
     * @return A string representation of the object.
     */
    @Override
    public String toString() {
        SB.setLength(0);
        SB.append("Showing{")
            .append("dateTime=").append(dateTime)
            .append(", movie=").append(movie)
            .append('}');
        return SB.toString();
    }
}
