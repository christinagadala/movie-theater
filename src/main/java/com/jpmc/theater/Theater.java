package com.jpmc.theater;

import com.google.gson.*;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Represents a movie theater with its name, a list of showings, and a list of reservations.
 */
public class Theater {

    private final String name;
    private final List<Showing> showings;
    private final List<Reservation> reservations;
    private static final double SPECIAL_MOVIE_DISCOUNT = 0.2;
    private static final int FIRST_SHOWING_DISCOUNT = 3;
    private static final int SECOND_SHOWING_DISCOUNT = 2;
    private static final double TIME_DISCOUNT = 0.25;
    private static final int SEVENTH_DISCOUNT = 1;
    private static final JsonPrimitive JSON_PRIMITIVE;
    private static final Gson GSON;
    private static final StringBuilder SB;

    static {
        JSON_PRIMITIVE = new JsonPrimitive("yyyy-MM-dd'T'HH:mm");
        GSON = createCustomGson();
        SB = new StringBuilder();
    }

    /**
     * Constructs a new theater with the specified name, list of showings, and list of reservations.
     *
     * @param name         The name of the theater.
     * @param showings     A list of showings in the theater.
     * @param reservations A list of reservations in the theater.
     */
    public Theater(String name, List<Showing> showings, List<Reservation> reservations) {
        this.name = name;
        this.showings = new ArrayList<>(showings);
        this.reservations = new ArrayList<>(reservations);
    }

    /**
     * Get the theater name
     *
     * @return The name of the theater.
     */
    String getName() {
        return name;
    }

    /**
     * Get the theater showings
     *
     * @return The showings for the theater.
     */
    List<Showing> getShowings() {
        return showings.stream()
            .sorted(Comparator.comparing(Showing::getDateTime))
            .collect(Collectors.toList());
    }

    /**
     * Get the showings for a specific date in the theater.
     *
     * @param date The date for which to retrieve the showings.
     * @return A list of showings for the specified date.
     */
    List<Showing> getShowingsForDate(LocalDate date) {
        return showings.stream()
            .filter(showing -> showing.getDateTime().toLocalDate().equals(date))
            .collect(Collectors.toList());
    }

    /**
     * Get the reservations for a specific customer in the theater.
     *
     * @param customer The customer for which to retrieve the reservations.
     * @return A list of reservations for the specified customer.
     */
    List<Reservation> getReservationsForCustomer(Customer customer) {
        List<Reservation> customerReservations = new ArrayList<>();
        for (Reservation reservation : reservations) {
            if (reservation.getCustomer().equals(customer)) {
                customerReservations.add(reservation);
            }
        }
        return Collections.unmodifiableList(customerReservations);
    }

    /**
     * Generates the movie schedule in JSON format with the theater name and sorted showings included.
     *
     * @return The movie schedule in JSON format with the theater name and sorted showings.
     */
    String getMovieScheduleJson() {
        return GSON.toJson(this);
    }

    String getMovieScheduleText() {
        SB.setLength(0);
        List<Showing> sortedShowings = getShowings();
        LocalDate currentDate = null;
        for (Showing showing : sortedShowings) {
            LocalDate showingDate = showing.getDateTime().toLocalDate();
            if (!showingDate.equals(currentDate)) {
                if (currentDate != null) {
                    SB.append("===================================================\n");
                }
                SB.append(showingDate).append("\n");
                SB.append("===================================================\n");
                currentDate = showingDate;
            }
            SB.append(getShowingEntry(showing)).append("\n");
        }
        SB.append("===================================================\n");
        return SB.toString();
    }


    /**
     * Calculates the ticket price for a showing based on various discounts.
     *
     * @param showing The showing for which to calculate the ticket price.
     * @return The final ticket price after applying discounts. If multiple discounts apply,
     * then return the maximum discount (minimum price)
     */
    double calculateTicketPrice(Showing showing) {
        double baseTicketPrice = showing.getMovie().getBaseTicketPrice();
        LocalDate reservationDate = showing.getDateTime().toLocalDate();
        double finalPrice = baseTicketPrice;

        // Apply discount for special movie
        if (isSpecialMovie(showing.getMovie())) {
            finalPrice = Math.min(baseTicketPrice, baseTicketPrice - baseTicketPrice * SPECIAL_MOVIE_DISCOUNT);
        }

        // Apply discount for first showing of the day
        if (getShowingIndex(reservationDate, showing) == 0) {
            finalPrice = Math.min(finalPrice, baseTicketPrice - FIRST_SHOWING_DISCOUNT);
        }

        // Apply discount for second showing of the day
        if (getShowingIndex(reservationDate, showing) == 1) {
            finalPrice = Math.min(finalPrice, baseTicketPrice - SECOND_SHOWING_DISCOUNT);
        }

        // Apply discount for showings between 11 AM and 4 PM
        if (isShowingBetween11AMAnd4PM(showing)) {
            finalPrice = Math.min(finalPrice, baseTicketPrice - baseTicketPrice * TIME_DISCOUNT);
        }

        // Apply discount for showings on the 7th
        if (isTheSeventh(showing.getDateTime())) {
            finalPrice = Math.min(finalPrice, baseTicketPrice - SEVENTH_DISCOUNT);
        }

        return finalPrice;
    }

    /**
     * Adds a reservation to the list of the theater's reservations.
     *
     * @return Whether the reservation was successfully added
     */
    boolean makeReservation(Reservation reservation) {
        reservations.add(reservation);
        return reservations.contains(reservation);
    }

    /**
     * Returns a hash code value for the object.
     *
     * @return A hash code value for this object.
     */
    @Override
    public int hashCode() {
        return Objects.hash(name, showings);
    }

    /**
     * Indicates whether some other object is "equal to" this one.
     *
     * @param o The reference object with which to compare.
     * @return {@code true} if this object is the same as the obj argument;
     * {@code false} otherwise.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Theater other = (Theater) o;
        return Objects.equals(name, other.name) &&
            Objects.equals(showings, other.showings);
    }

    /**
     * Returns a string representation of the object.
     *
     * @return A string representation of the object.
     */
    @Override
    public String toString() {
        return getMovieScheduleText();
    }

    /**
     * Checks if the specified movie is a special movie.
     *
     * @param movie The movie to check
     * @return {@code true} if the movie is a special movie; otherwise, {@code false}.
     */
    private static boolean isSpecialMovie(Movie movie) {
        return movie.getTitle().equals("Special Movie"); // Example implementation
    }

    /**
     * Gets the showing index for the given date.
     *
     * @param date    The date for which to get the showing index.
     * @param showing The showing for which we want to get the index.
     * @return The showing index, or -1 if the showing does not exist for that date.
     */
    private int getShowingIndex(LocalDate date, Showing showing) {
        List<Showing> showings = getShowingsForDate(date);
        return showings.indexOf(showing);
    }

    /**
     * Checks if the specified start time is between 11 AM and 4 PM.
     *
     * @param showing The showing to check.
     * @return {@code true} if the start time is between 11 AM and 4 PM; otherwise, {@code false}.
     */
    private static boolean isShowingBetween11AMAnd4PM(Showing showing) {
        LocalTime showingTime = showing.getDateTime().toLocalTime();
        LocalTime showingTime11AM = LocalTime.of(11, 0);
        LocalTime showingTime4PM = LocalTime.of(16, 0);
        return !showingTime.isBefore(showingTime11AM) && showingTime.isBefore(showingTime4PM);
    }

    /**
     * Checks if the specified date is the 7th.
     *
     * @param date The date to check
     * @return {@code true} if the date is on the 7th {@code false}.
     */
    private static boolean isTheSeventh(LocalDateTime date) {
        return date.getDayOfMonth() == 7;
    }

    /**
     * Creates a custom Gson instance with a JsonSerializer for LocalDateTime objects that converts them to
     * a custom date-time format. Exclude reservations since we don't want to expose this in the string representation
     *
     * @return The custom Gson instance with the registered serializer.
     */
    private static Gson createCustomGson() {
        JsonSerializer<LocalDateTime> localDateTimeSerializer = (dateTime, typeOfSrc, context) ->
            new JsonPrimitive(dateTime.format(DateTimeFormatter.ofPattern(JSON_PRIMITIVE.getAsString())));

        // Custom JsonSerializer for Duration
        JsonSerializer<Duration> durationSerializer = (duration, typeOfSrc, context) -> {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("seconds", duration.getSeconds());
            return jsonObject;
        };

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(LocalDateTime.class, localDateTimeSerializer);
        gsonBuilder.registerTypeAdapter(Duration.class, durationSerializer);

        // Exclude the "reservations" field from the serialization process
        gsonBuilder.setExclusionStrategies(new ExclusionStrategy() {
            @Override
            public boolean shouldSkipField(FieldAttributes f) {
                return f.getName().equals("reservations");
            }

            @Override
            public boolean shouldSkipClass(Class<?> clazz) {
                return false;
            }
        });

        return gsonBuilder.create();
    }

    /**
     * Formats a single showing entry for the movie schedule.
     *
     * @param showing The showing to format.
     * @return The formatted showing entry.
     */
    private String getShowingEntry(Showing showing) {
        Duration duration = showing.getMovie().getDuration();
        LocalDate showingDate = showing.getDateTime().toLocalDate();
        double ticketPrice = calculateTicketPrice(showing);
        return String.format("%d: %s %s (%s) $%.1f",
            getShowingIndex(showingDate, showing) + 1,
            showing.getDateTime(),
            showing.getMovie().getTitle(),
            formatDuration(duration),
            ticketPrice);
    }

    /**
     * Formats the duration as "hours minutes" for display.
     *
     * @param duration The duration to format.
     * @return The formatted duration as a string.
     */
    private String formatDuration(Duration duration) {
        long hours = duration.toHours();
        long minutes = duration.toMinutes() % 60;
        return String.format("%d hour %d minutes", hours, minutes);
    }
}
