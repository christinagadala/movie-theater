package com.jpmc.theater;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CustomerTest {

  private static Theater theater;

  @BeforeAll
  static void setUp() {
    theater = new Theater("Test Theater",
        List.of(
            new Showing(LocalDateTime.parse("2023-07-30T07:00"), new Movie("Movie A", Duration.ofMinutes(90), 12.0)),
            new Showing(LocalDateTime.parse("2023-07-30T11:00"), new Movie("Movie A", Duration.ofMinutes(90),12.0)),
            new Showing(LocalDateTime.parse("2023-07-31T09:00"), new Movie("Movie A", Duration.ofMinutes(90),12.0))
        ),
        Collections.emptyList()
    );
  }

  @ParameterizedTest
  @CsvSource({
      "Jane, Movie A, 90, 12.0, 2023-07-30T07:00, true", // Test that a reservation can be made if the theater has the showing
      "John, Movie B, 90, 15.0, 2023-07-31T14:30, false" // Test that a reservation cannot be made if the theater does not have the showing
  })
  void testReserveShowing(
      String customerName,
      String movieTitle,
      long durationMinutes,
      double baseTicketPrice,
      String showingDateTimeStr,
      boolean expectedWasReserved
  ) {
    boolean resultWasReserved = new Customer(customerName).reserveShowing(
        theater,
        new Showing(LocalDateTime.parse(showingDateTimeStr), new Movie(movieTitle, Duration.ofMinutes(durationMinutes), baseTicketPrice))
    );
    assertEquals(expectedWasReserved, resultWasReserved, "The result showing whether the reservation was made should match the expectation.");
  }
}
