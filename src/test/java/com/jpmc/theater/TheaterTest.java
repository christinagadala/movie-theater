package com.jpmc.theater;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TheaterTest {

    // Used across tests
    private static Theater theater;

    @BeforeAll
    static void setUp() {
        Showing showing1 = new Showing(LocalDateTime.parse("2023-07-30T07:00"), new Movie("Movie A", Duration.ofMinutes(90), 12.0)); // first showing discount
        Showing showing2 = new Showing(LocalDateTime.parse("2023-07-30T11:00"), new Movie("Movie A", Duration.ofMinutes(90), 12.0)); // time discount
        Showing showing3 = new Showing(LocalDateTime.parse("2023-07-31T09:00"), new Movie("Movie A", Duration.ofMinutes(90), 12.0)); // first showing discount
        theater = new Theater("Test Theater",
            List.of(
                showing1,
                showing2,
                showing3
            ),
            List.of(
                new Reservation(showing1, new Customer("Jane")),
                new Reservation(showing2, new Customer("John")),
                new Reservation(showing3, new Customer("Jill"))
            )
        );
    }

    @ParameterizedTest
    @CsvSource({
        "John, 1", // John has a reservation shown in the setup above
        "Jane, 1", // Jill has a reservation shown in the setup above
        "Joe, 0" // Joe does not have a reservation since he is not shown in the setup above
    })
    void testGetReservationsForCustomer(String customerName, int expectedReservationCount) {
        Customer customer = new Customer(customerName);
        List<Reservation> reservations = theater.getReservationsForCustomer(customer);

        // Test that the number of reservations is correct for each customer after the reservations were made in the test before
        assertEquals(expectedReservationCount, reservations.size(), "Reservation count should match the expected count.");
    }

    @ParameterizedTest
    @CsvSource({
        "2023-07-30, 2", // date exists, should be 2
        "2023-07-31, 1", // date exists in showings so we should have a count of 1
        "2023-08-02, 0" // date does not exist in showings so we should have a count of 0
    })
    void testGetShowingsForDate(LocalDate date, int expectedShowingsCount) {
        List<Showing> result = theater.getShowingsForDate(date);
        assertEquals(expectedShowingsCount, result.size(), "Number of showings for the specified date should match the expected count.");
    }

    @ParameterizedTest
    @CsvSource({
        "2023-07-30, 2", // date exists, should be 2
        "2023-07-31, 1", // date exists in showings so we should have a count of 1
        "2023-08-02, 0" // date does not exist in showings so we should have a count of 0
    })
    void testGetShowingsForMovie(LocalDate date, int expectedShowingsCount) {
        List<Showing> result = theater.getShowingsForDate(date);
        assertEquals(expectedShowingsCount, result.size(), "Number of showings for the specified date should match the expected count.");
    }

    @ParameterizedTest
    @CsvSource({ // Test data for theater with showings and discount scenarios
        "Test Theater 1, Special Movie, 90, 15.0, 10:30, 13:15, 15:30, 18:00, 2023-07-28T10:30:00, 12.00", // Special movie discount (20%)
        // Apply time discount which has greater discount
        "Test Theater 1, Special Movie, 90, 15.0, 10:30, 13:15, 15:30, 18:00, 2023-07-28T14:00:00, 11.25", // Special movie discount (20%) and  Time discount (25%)
        "Test Theater 1, Normal Movie, 90, 12.0, 07:00, 09:30, 14:00, 17:00, 2023-07-28T07:00:00, 9.0", // First showing discount ($3)
        "Test Theater 1, Normal Movie, 90, 12.0, 07:00, 09:30, 14:00, 17:00, 2023-07-28T09:30:00, 10.0", // Second showing discount ($2)
        "Test Theater 1, Normal Movie, 90, 12.0, 07:00, 09:30, 14:00, 17:00, 2023-07-28T14:00:00, 9.0", // Time discount (25%)
        "Test Theater 2, Normal Movie, 90, 14.0, 09:45, 11:00, 15:15, 18:30, 2023-07-28T18:30:00, 14.0", // No discount
        "Test Theater 2, Normal Movie, 90, 14.0, 09:45, 11:00, 15:15, 18:30, 2023-07-28T09:45:00, 11.0", // First showing discount ($3)
        // Apply time discount which has greater discount
        "Test Theater 2, Normal Movie, 90, 14.0, 09:45, 11:00, 15:15, 18:30, 2023-07-28T11:00:00, 10.5", // Second showing discount ($2) and Time discount (25%)
        "Test Theater 2, Normal Movie, 90, 14.0, 09:45, 11:00, 15:15, 18:30, 2023-07-28T15:15:00, 10.5", // Time discount (25%)
        "Test Theater 2, Normal Movie, 90, 14.0, 09:45, 11:00, 15:15, 18:30, 2023-07-07T18:15:00, 13", // 7th discount ($1)
    })
    void testCalculateTicketPrice(
        String theaterName,
        String movieTitle,
        long durationMinutes,
        double baseTicketPrice,
        String showingTime1,
        String showingTime2,
        String showingTime3,
        String showingTime4,
        String reservationTimeString,
        double expectedPrice
    ) {
        Movie movie = new Movie(movieTitle, Duration.ofMinutes(durationMinutes), baseTicketPrice);
        LocalDateTime reservationTime = LocalDateTime.parse(reservationTimeString);
        List<Showing> showings = new ArrayList<>();
        showings.add(new Showing(LocalDateTime.of(reservationTime.toLocalDate(), LocalTime.parse(showingTime1)), movie));
        showings.add(new Showing(LocalDateTime.of(reservationTime.toLocalDate(), LocalTime.parse(showingTime2)), movie));
        showings.add(new Showing(LocalDateTime.of(reservationTime.toLocalDate(), LocalTime.parse(showingTime3)), movie));
        showings.add(new Showing(LocalDateTime.of(reservationTime.toLocalDate(), LocalTime.parse(showingTime4)), movie));
        Showing showing = new Showing(reservationTime, movie);
        Theater theater = new Theater(theaterName, showings, Collections.emptyList());
        double actualPrice = theater.calculateTicketPrice(showing);
        // Check if the ticket price is non-negative
        Assertions.assertTrue(actualPrice >= 0, "Ticket price should be non-negative");
        // Check if the ticket price is not greater than the base ticket price
        Assertions.assertTrue(actualPrice <= baseTicketPrice, "Ticket price should not be greater than the base ticket price");
        // Check the calculated price based on the expected price in the test data
        Assertions.assertEquals(expectedPrice, actualPrice, 0.01, "Incorrect ticket price calculation");
    }

    @Test
    void testGetMovieScheduleJsonAndGetMovieScheduleText() {

        // Call the getMovieScheduleJson method to generate the JSON representation
        String movieScheduleJson = theater.getMovieScheduleJson();

        Showing showing1 = theater.getShowings().get(0);
        Showing showing2 = theater.getShowings().get(1);
        Showing showing3 = theater.getShowings().get(2);


        // Call the getMovieScheduleText method to generate the movie schedule in text format
        String movieScheduleText = theater.getMovieScheduleText();

        String expectedJson =
            "{\"name\":\"" + theater.getName() + "\",\"showings\":[" +
                formatJsonEntry(showing1.getDateTime(), showing1.getMovie().getTitle(), showing1.getMovie().getDuration(), showing1.getMovie().getBaseTicketPrice(), false) +
                formatJsonEntry(showing2.getDateTime(), showing2.getMovie().getTitle(), showing1.getMovie().getDuration(), showing2.getMovie().getBaseTicketPrice(), false) +
                formatJsonEntry(showing3.getDateTime(), showing3.getMovie().getTitle(), showing1.getMovie().getDuration(), showing3.getMovie().getBaseTicketPrice(), true) +
                "]}";

        String expectedMovieScheduleText = "2023-07-30\n" +
            "===================================================\n" +
            "1: 2023-07-30T07:00 Movie A (1 hour 30 minutes) $9.0\n" +
            "2: 2023-07-30T11:00 Movie A (1 hour 30 minutes) $9.0\n" +
            "===================================================\n" +
            "2023-07-31\n" +
            "===================================================\n" +
            "1: 2023-07-31T09:00 Movie A (1 hour 30 minutes) $9.0\n" +
            "===================================================\n";

        // Compare the generated movie schedule text with the expected value
        assertEquals(expectedMovieScheduleText, movieScheduleText, "Generated movie schedule text should match the expected value.");

        // Compare the generated JSON with the expected value
        assertEquals(expectedJson, movieScheduleJson, "Generated JSON should match the expected JSON value.");
    }

    // Helper method to format JSON entries for the movie schedule
    private static String formatJsonEntry(LocalDateTime dateTime, String movieTitle, Duration duration, double baseTicketPrice, boolean isLast) {
        String jsonEntry = String.format("{\"dateTime\":\"%s\",\"movie\":{\"title\":\"%s\",\"duration\":{\"seconds\":%d},\"baseTicketPrice\":%.1f}}",
            dateTime, movieTitle, duration.getSeconds(), baseTicketPrice);

        // Add a comma at the end of the JSON entry if it's not the last entry
        return isLast ? jsonEntry : jsonEntry + ",";
    }
}
